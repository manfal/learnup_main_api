# Migration commands

    python -m src.models.migration db init
    python -m src.models.migration db migrate
    python -m src.models.migration db upgrade

# Seeding command

    python -m src.models.seed

# Translation commands

    pybabel extract -F babel.cfg -o src\language\language.pot src
    pybabel init -i src\language\language.pot -d src\translations -l es
    pybabel compile -d src\translations
