from config import HOST, PORT
from src import app, socket


if '__main__' == __name__:
    socket.run(app, host=HOST, port=PORT)
