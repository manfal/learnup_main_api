from .. import babel
from flask import request
from config import LANGUAGES
from .. import app

#   Services
from src.services.university_config import service_university_config
#   Services

#   Configurations


@babel.localeselector
def select_locale():
    return request.accept_languages.best_match(LANGUAGES.keys())
#   Configurations

#   Service registration to main app
app.register_blueprint(service_university_config)
#   Service registration to main app
