from flask_sqlalchemy import SQLAlchemy
from src import app

db = SQLAlchemy(app)

import models.CourseSections
import models.EducationFormat
import models.Faculties
import models.GradeMode
import models.GradeScale
import models.UniversityConfig
import models.Logs


