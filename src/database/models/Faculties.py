from .. import db


class Faculties(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    faculty_name = db.Column(db.String(250), nullable=False)
    faculty_code = db.Column(db.String(100), nullable=False, unique=True, index=True)
    time_created = db.Column(db.DateTime())
    author = db.Column(db.String(250), nullable=False)

    def __init__(self, faculty_name, faculty_code, time_created, author):
        self.faculty_name = faculty_name
        self.faculty_code = faculty_code
        self.time_created = time_created
        self.author = author
