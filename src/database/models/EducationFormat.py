from .. import db


class EducationFormat(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    format_code = db.Column(db.String(100), nullable=False, index=True, unique=True)
    format_string = db.Column(db.String(250), nullable=False)
    time_created = db.Column(db.DateTime())
    author = db.Column(db.String(100), nullable=True)

    def __init__(self, format_code, format_string, time_created, author=None):
        self.format_code = format_code
        self.format_string = format_string
        self.time_created = time_created
        if author is not None:
            self.author = author
