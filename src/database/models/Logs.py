from .. import db


class Logs(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    function_name = db.Column(db.String(250))
    level_name = db.Column(db.String(100), nullable=False)
    line_number = db.Column(db.Integer, nullable=False)
    message = db.Column(db.String(250), nullable=False)
    path_name = db.Column(db.String(250), nullable=False)

    def __init__(self, function_name, level_name, line_number, message, path_name):
        self.function_name = function_name
        self.level_name = level_name
        self.line_number = line_number
        self.message = message
        self.path_name = path_name
