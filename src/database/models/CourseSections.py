from .. import db


class CourseSections(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    section_name = db.Column(db.String(250), nullable=False)
    time_created = db.Column(db.DateTime())
    author = db.Column(db.String(100), nullable=False)

    def __init__(self, section_name, time_created, author):
        self.section_name = section_name
        self.time_created = time_created
        self.author = author
