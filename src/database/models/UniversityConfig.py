from .. import db


class UniversityConfig(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    config_key = db.Column(db.String(250), nullable=False, index=True, unique=True)
    config_value = db.Column(db.String(250))
    config_title = db.Column(db.String(250), nullable=False)
    time_created = db.Column(db.DateTime())
    author = db.Column(db.String(100), nullable=False)

    def __init__(self, config_key, config_value, config_title, time_created, author):
        self.config_key = config_key
        self.config_value = config_value
        self.config_title = config_title
        self.time_created = time_created
        self.author = author
