from .. import db


class GradeScale(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    grade_code = db.Column(db.String(100), nullable=False, unique=True, index=True)
    grade_value = db.Column(db.String(100), nullable=False)
    time_created = db.Column(db.DateTime())
    author = db.Column(db.String(100), nullable=True)

    def __init__(self, grade_code, grade_value, time_created, author):
        self.grade_code = grade_code
        self.grade_value = grade_value
        self.time_created = time_created
        self.author = author
