from .. import db


class GradeMode(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    grade_code = db.Column(db.String(100), nullable=False, index=True, unique=True)
    grade_string = db.Column(db.String(250), nullable=False)
    time_created = db.Column(db.DateTime())
    author = db.Column(db.String(100), nullable=True)

    def __init__(self, grade_code, grade_string, time_created, author=None):
        self.grade_code = grade_code
        self.grade_string = grade_string
        self.time_created = time_created
        if author is not None:
            self.author = author
