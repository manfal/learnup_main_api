from . import db
from models.UniversityConfig import UniversityConfig
from models.CourseSections import CourseSections
from models.EducationFormat import EducationFormat
from models.Faculties import Faculties
from models.GradeScale import GradeScale
from models.GradeMode import GradeMode
from ..dependencies.dependency_list import utilities

#   Grade modes available by default for universities to chose from


percentage_mode = GradeMode('PERCENTAGE', 'Percentage', utilities.date_time.get_current_time())
gpa_mode = GradeMode('GPA', 'GPA', utilities.date_time.get_current_time())

db.session.add(percentage_mode)
db.session.add(gpa_mode)

#   Education formats allowed in university, available by default.


annual_format = EducationFormat('ANNUAL', 'Annual', utilities.date_time.get_current_time())
semester_format = EducationFormat('SEMESTER', 'Semester', utilities.date_time.get_current_time())

db.session.add(annual_format)
db.session.add(semester_format)

#   Course sections information


pre_mid = CourseSections('Pre mid', utilities.date_time.get_current_time(), 'anfal')
post_mid = CourseSections('Post mid', utilities.date_time.get_current_time(), 'anfal')

db.session.add(pre_mid)
db.session.add(post_mid)

#   University configurations (dummy for GIKI)


grade_mode = UniversityConfig('GRADE_MODE', 'GPA', 'GPA', utilities.date_time.get_current_time(), 'anfal')
gpa_config = UniversityConfig('MAX_GPA', '4', 'Max GPA', utilities.date_time.get_current_time(), 'anfal')
decimal_place_config = UniversityConfig(
    'DECIMAL_ALLOWED',
    '2',
    'Decimal Places Allowed',
    utilities.date_time.get_current_time(),
    'anfal'
)
education_format_string = UniversityConfig(
    'EDUCATION_FORMAT',
    'SEMESTER',
    'Semester Format',
    utilities.date_time.get_current_time(),
    'anfal'
)

db.session.add(grade_mode)
db.session.add(gpa_config)
db.session.add(decimal_place_config)
db.session.add(education_format_string)

#   Grade scale information.


a_plus = GradeScale('A+', '4', utilities.date_time.get_current_time(), 'anfal')
a = GradeScale('A', '3.67', utilities.date_time.get_current_time(), 'anfal')
a_negative = GradeScale('A-', '3.5', utilities.date_time.get_current_time(), 'anfal')
b_plus = GradeScale('B+', '3', utilities.date_time.get_current_time(), 'anfal')
b = GradeScale('B', '2.67', utilities.date_time.get_current_time(), 'anfal')
b_negative = GradeScale('B-', '2.5', utilities.date_time.get_current_time(), 'anfal')
c_plus = GradeScale('C+', '2.2', utilities.date_time.get_current_time(), 'anfal')
c = GradeScale('C', '2', utilities.date_time.get_current_time(), 'anfal')
c_negative = GradeScale('C-', '1.8', utilities.date_time.get_current_time(), 'anfal')
d = GradeScale('D', '1.5', utilities.date_time.get_current_time(), 'anfal')
f = GradeScale('F', '1.2', utilities.date_time.get_current_time(), 'anfal')

db.session.add(a_plus)
db.session.add(a)
db.session.add(a_negative)
db.session.add(b_plus)
db.session.add(b)
db.session.add(b_negative)
db.session.add(c_plus)
db.session.add(c)
db.session.add(c_negative)
db.session.add(d)
db.session.add(f)

#   Commit the data

#   Faculties Info

fcse = Faculties('Faculty of Computer Sciences & Engineering', 'FCSE', utilities.date_time.get_current_time(), 'anfal')
fes = Faculties('Faculty of Engineering Sciences', 'FES', utilities.date_time.get_current_time(), 'anfal')

db.session.add(fcse)
db.session.add(fes)

#   Faculties Info

db.session.commit()
