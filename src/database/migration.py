from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager
from . import db
from src import app

migrate = Migrate(app, db)
manager = Manager(app)
manager.add_command('db', MigrateCommand)

manager.run()
