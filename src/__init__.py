from flask import Flask
from flask_babel import Babel
from flask_restful import Api
from flask_socketio import SocketIO
from flask_cors import CORS

app = Flask(__name__)
app.config.from_object('config')
cors = CORS(app)
babel = Babel(app)
restful = Api(app)

#   Although this value should be in the notification system dependency
#   I am adding it here because I will be using this instead of standard
#   flask run method to run the app.
#   It also provides the support for running the app on production ready eventlet
#   server.
socket = SocketIO(app)

from database import db
from api import routes
