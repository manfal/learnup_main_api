from src import app, socket
from src.database import db
from src.dependencies.dependency_list import logger
from src.dependencies.dependency_list import utilities

#   University Config dependencies
from src.database.models.CourseSections import CourseSections as CourseSectionsModel
from src.database.models.EducationFormat import EducationFormat as EducationFormatModel
from src.database.models.Faculties import Faculties as FacultiesModel
from src.database.models.GradeMode import GradeMode as GradeModeModel
from src.database.models.GradeScale import GradeScale as GradeScaleModel
from src.database.models.UniversityConfig import UniversityConfig as UniversityConfigModel
#   University Config dependencies
