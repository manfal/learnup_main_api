#   Some app level variables dependencies might need.
from src import app
from src.database import db
#   Some app level variables dependencies might need.

#   Some other dependencies for dependencies
from src.database.models.Logs import Logs
#   Some other dependencies for dependencies

#   Dependency imports
from logger.controllers.Logger import Logger
from learnup_utilities.controllers.Utilities import Utilities
#   Dependency imports

#   Logger dependency
logger = Logger(app=app, db=db, log_table=Logs).get_logger()
#   Logger dependency

#   Utility dependency
utilities = Utilities()
#   Utility dependency

