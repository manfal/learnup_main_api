#   System information
VERSION_CODE = '1.0.0'
HOST = '0.0.0.0'
PORT = 5000

#   Decides whether the application should run in debug mode
DEBUG = True

#   ORM configurations.
SQLALCHEMY_DATABASE_URI = 'mysql://anfal:anfal@localhost/main_api'
SQLALCHEMY_TRACK_MODIFICATIONS = False

#   Babel configurations for language
LANGUAGES = {
    'en': 'English'
}

#   Flask Restful configurations
ERROR_404_HELP = False

#   Flask Restful Request parser configurations
BUNDLE_ERRORS = True
